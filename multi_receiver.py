
'''
Top-level script to manipulate multiple receivers

Copyright Ben Nizette 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.'''

import sys
import argparse
import threading
import datetime

import Ephemeris_Model
import Range_Model
import Simple_Solver

import null_solver

import kml_output as kml
import statistic_output as stat_out

from UBX_Module import UBX_Module


def run_solver(solver, sinks, iterations):
    for i in range(0, iterations):
        print("id {} iter {}".format(solver.ident, i))

        try:
            s = solver.solve()
        except Simple_Solver.UnsolveableException as e:
            print("id {} iter {} UNSOLVEABLE: {}".format(solver.ident, i, str(e)))
        else:
            for sink in sinks:
                sink.register_point(solver.ident, s)

parser = argparse.ArgumentParser()

parser.add_argument('--port', action='append',
                    help="Serial port(s) to which the receiver(s) are connected")
parser.add_argument('--kml',
                    help="Write points to a KML file of given filename")
parser.add_argument('--iterations', default=100, type=int,
                    help="Number of iterations to run for.  Includes unsuccessful solution runs")
parser.add_argument('--stats',
                    help="Write statistics to the given file or '-' for stdout")
parser.add_argument('--log-all', action='store_true',
                    help="Write verbose debugging to a set of log files")
args = parser.parse_args()

modules = []
solvers = []

eph = Ephemeris_Model.Ephemeris()

for i, port in enumerate(args.port):
    if args.log_all:
        lfile = datetime.datetime.now().strftime("%y%m%d-%H%M") + "-" + str(i)
    else:
        lfile = None

    loc_model = Range_Model.Corrected_Rangeset(eph, 0, logfile=lfile)
    module = UBX_Module(port, i, loc_model, logfile=lfile)
    solver = Simple_Solver.Simple_Solver(i, module.loc, logfile=lfile)

    modules.append(module)
    solvers.append(solver)

sinks = []

if args.kml is not None:
    kml_sink = kml.KML_Output(args.kml)
    sinks.append(kml_sink)

if args.stats is not None:
    stat_sink = stat_out.Stat_Output(args.stats)
    sinks.append(stat_sink)

for sink in sinks:
    sink.prepare()

threads = []
    
for solver in solvers:
    t = threading.Thread(target=run_solver, args=(solver, sinks, args.iterations))
    threads.append(t)

    t.start()


for t in threads:
    t.join()

print("++")

for sink in sinks:
    sink.finalise()

for module in modules:
    module.stop()


