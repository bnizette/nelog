'''
Top level script to manipulate a single receiver

Copyright Ben Nizette 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.'''

import sys
import argparse
import datetime

import Ephemeris_Model
import Range_Model
import Simple_Solver

import null_solver

import kml_output as kml
import statistic_output as stat_out

from UBX_Module import UBX_Module

parser = argparse.ArgumentParser()

parser.add_argument('--port', default="COM8",
                    help="Serial port to which the receiver is connected")
parser.add_argument('--kml',
                    help="Write points to a KML file of given filename")
parser.add_argument('--iterations', default=100, type=int,
                    help="Number of iterations to run for.  Includes unsuccessful solution runs")
parser.add_argument('--progress', action="store_true",
                    help="Show basic progress bar")
parser.add_argument('--mod_solution', action="store_true",
                    help="Use the module's internal solution, don't calculate one ourselves")
parser.add_argument('--stats',
                    help="Write statistics to the given file or '-' for stdout")
parser.add_argument('--log-all', action='store_true',
                    help="Write verbose debugging to a set of log files")
args = parser.parse_args()

if args.log_all:
    lfile = datetime.datetime.now().strftime("%y%m%d-%H%M") + "-" + str(0)
else:
    lfile = None


eph_model = Ephemeris_Model.Ephemeris()
loc_model = Range_Model.Corrected_Rangeset(eph_model, 0, logfile=lfile)
module = UBX_Module(args.port, 0, loc_model, logfile=lfile)

if args.mod_solution:
    solver = null_solver.Null_Solver(0, module.loc)
else:
    solver = Simple_Solver.Simple_Solver(0, module.loc, logfile=lfile)

sinks = []

if args.kml is not None:
    kml_sink = kml.KML_Output(args.kml)
    sinks.append(kml_sink)

if args.stats is not None:
    stat_sink = stat_out.Stat_Output(args.stats)
    sinks.append(stat_sink)

for sink in sinks:
    sink.prepare()

pstep = args.iterations / 20

if args.progress:
    print('[', end=''), sys.stdout.flush()
    
for i in range(0, args.iterations):
    if i % pstep == 0 and args.progress:
        print('.', end=''), sys.stdout.flush()

    try:
        s = solver.solve()
    except Simple_Solver.UnsolveableException:
        pass
    except (KeyboardInterrupt, SystemExit):
        for sink in sinks:
            sink.finalise()
        raise
    else:
        for sink in sinks:
            sink.register_point(1, s)

if args.progress:
    print(']'), sys.stdout.flush()

for sink in sinks:
    sink.finalise()

module.stop()
