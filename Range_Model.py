
'''
Representations of sets of distances from a single receiver to satellites

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import threading

from collections import namedtuple

from util import *

F = -4.442807633 * (10**(-10))
T_amb = 20
P_amb = 101
P_vap = 0.86

class SV_Measurement(namedtuple('SV_Measurement', 'gps_time raw_range doppler phase prange')):
    pass

class Raw_Rangeset:
    ''' A set of uncorrected PR data, smoothed using a Hatch filter '''
    def __init__(self, ident, logfile=None):
        self.ionosphere = []
        self.pranges = {}
        self.mod_solution = [0,0,0]

        self.filter_state = {}

        self.cond = threading.Condition()

        self.logfile = None
        if logfile:
            self.logfile = logfile + "-RAN-" + str(ident) + ".dump"


    def _write_logfile(self, line):
        if self.logfile is None:
            return

        with open(self.logfile, 'a') as f:
            f.write(line)


    def _pr_smooth(self, sv, pr, phase):
        ''' Hatch Filter based on http://home-2.worldonline.nl/~samsvl/smooth.htm '''
        max_obs = 100
        max_slip = 15

        if not sv in self.filter_state:
            self.filter_state[sv] = {}
            self.filter_state[sv]['C'] = phase
            self.filter_state[sv]['P'] = pr
            self.filter_state[sv]['S'] = pr
            self.filter_state[sv]['N'] = 0

        s = self.filter_state[sv]

        s['N'] = min(s['N'] + 1, max_obs)

        if s['N'] > 1:
            slip = abs((pr - s['P']) - (phase - s['C']))
        else:
            slip = 0

        if slip > max_slip:
            s['N'] = 1

        if s['N'] == 1:
            s['S'] = pr
        else:
            s['S'] = pr / s['N'] + (s['S'] + phase - s['C']) * (s['N'] - 1) / s['N']

        s['P'] = pr
        s['C'] = phase

        self._write_logfile("PR SMOOTH SAT {} COUNT {} SLIP {}\n".format(sv, s['N'], slip))

        return s['S']

    def load_ionospheric_entry(self, params):
        self.cond.acquire()
        self.ionosphere = params
        self._write_logfile("IONOSPHERE {}\n".format(params))
        self.cond.release()

    def load_mod_solution(self, solution):
        self.mod_solution = solution
        self._write_logfile("MOD SOLN {}\n".format(solution))

    def start_load(self):
        self.cond.acquire()
        # Clear the range set at each load, this way we don't
        # end up with stale entries when a sat drops out
        self.pranges = {}

    def end_load(self):
        self._write_logfile("PR LOAD {}\n".format(self.pranges))
        # Only notify data if our Ephemerides are valid in turn
        if self.eph.valid:
            self.cond.notifyAll()
        self.cond.release()

    def load_raw_entry(self, sv, time, prange, doppler, phase):
        srange = self._pr_smooth(sv, prange, phase)

        self.pranges[sv] = SV_Measurement(
                gps_time = time,
                raw_range = prange,
                prange = srange,
                doppler = doppler,
                phase = phase)

    def _get_ionosphere(self):
        return self.ionosphere

    def _get_pranges(self):
        return self.pranges

    def _get_prange_sv(self, sv):
        return self.pranges[sv]

    def get_sats(self):
        return set(self.pranges.keys())

    def get_gps_time(self, sv):
        return self.pranges[sv].gps_time


class Corrected_Rangeset(Raw_Rangeset):
    ''' A range set where each pseudorange is corrected for the following:
        - Tropospheric effects (Hopfield)
        - Ionospheric effects (Klobuchar)
        - Clock offset and group delay
        - Relativistic effects
        - Rotation of the Earth (in conjunction with the Ephemeris data) '''

    def __init__(self, eph, ident, logfile=None):
        ''' Unlike the uncorrected rangeset, this must be linked to an Ephemeris'''
        Raw_Rangeset.__init__(self, ident, logfile)
        self.pr_corr = {}
        self.est_pos = [0, 0, 0]
        self.eph = eph

    def _sat_clk_offset(self, sv):
        '''Computes the error in the satellite clock given the broadcast error parameters
           Returns the correction in seconds'''
        pr = self._get_prange_sv(sv).prange
        time = self._get_prange_sv(sv).gps_time
        clk_params = self.eph.get_sat_ephemeris(sv)

        Ttr = time - (pr / C)
        Toc = clk_params.toc
        af = [clk_params.af0, clk_params.af1, clk_params.af2]

        t = Ttr - Toc

        if t > 302400:
            t = t - 604800
        elif t < -302400:
            t = t + 604800

        off = 0
        for i in range(0, len(af)):
            off = off + af[i] * (t**(i))

        off = off - clk_params.Tgd

        return off

    def _sat_rel_offset(self, sv):
        '''Calculates the clock error due to relativistic effects
           Returns the correction in seconds'''
        e = self.eph.get_sat_ephemeris(sv)
        ek = self.eph.calc_sat_orbit(sv, self._get_prange_sv(sv).gps_time)[3]
        return F * e.e * e.rootA * sin(ek)

    def _tropospheric_correction(self, sv):
        '''This Function approximate Tropospheric Group Delay
           Reference:"GPS Theory and application",edited by B.Parkinson,J.Spilker

           Returns correction in meters'''

        [E, A] = calc_azimuth_elevation(self.est_pos, self.eph.get_sat_ECEF(
            sv, self._get_prange_sv(sv).gps_time))

        # Zenith Hydrostatic Delay
        Kd = 1.55208 * 10**(-4) * P_amb * (
            40136 + 148.72 * T_amb) / (T_amb + 273.16)

        # Zenith Wet Delay
        Kw = -0.282 * P_vap / (
            T_amb + 273.16) + 8307.2 * P_vap / (T_amb + 273.16)**2

        kdd = sin(sqrt(E**2 + 1.904 * 10**-3))
        kwd = sin(sqrt(E**2 + 0.6854 * 10**-3))
        return Kd / kdd + Kw/kwd  # Meter

    def _ionospheric_correction(self, sv):
        ''' Ionospheric (Klobuchar) correction for slant of the L1 frequency
            Based on code Copyright Moein Mehrtash 2008

            Returns correction in seconds '''
        GPS_Rcv = ECEF2GPS(self.est_pos)

        e = self.eph.get_sat_ephemeris(sv)

        iono = self._get_ionosphere()

        if (len(iono) != 8):
            return 0;

        a = iono[0:4]
        b = iono[4:8]

        Lat = GPS_Rcv[1] / pi
        Lon = GPS_Rcv[2] / pi  # semicircles unit Latitude and Longitude

        [El, A] = calc_azimuth_elevation(
            self.est_pos, self.eph.get_sat_ECEF(sv, self.get_gps_time(sv)))

        E = El / pi  # SemiCircle Elevation

        # Calculate the Earth-Centered angle, Psi
        Psi = 0.0137 / (E + 0.11) - 0.022

        # Compute the Subionospheric lattitude, Phi_L
        Phi_L = Lat + Psi * cos(A)  # SemiCircle

        if Phi_L > 0.416:
            Phi_L = 0.416
        elif Phi_L < -0.416:
            Phi_L = -0.416

        # Compute the subionospheric longitude, Lambda_L
        Lambda_L = Lon + (Psi * sin(A) / cos(Phi_L * pi))  # SemiCircle

        # Find the geomagnetic lattitude ,Phi_m, of the subionospheric location
        # looking toward each GPS satellite:
        Phi_m = Phi_L + 0.064 * cos((Lambda_L - 1.617) * pi)

        # Find the Local Time ,t, at the subionospheric point
        t = 4.23 * 10**4 * Lambda_L + self.get_gps_time(sv)

        if t > 86400:
            t = t - 86400
        elif t < 0:
            t = t + 86400

        # Convert Slant time delay, Compute the Slant Factor,F
        F = 1 + 16 * (0.53 - E**3)

        # Compute the ionospheric time delay T_iono by first computing x
        Per = b[0] + b[1] * Phi_m + b[2] * Phi_m**2 + b[3] * Phi_m**3

        if Per < 72000:
            Per = 72000

        x = 2 * pi * (t - 50400) / Per  # Rad

        AMP = a[0] + a[1] * Phi_m + a[2] * Phi_m**2 + a[3] * Phi_m**3

        if AMP < 0:
            AMP = 0

        if abs(x) > 1.57:
            T_iono = F * 5 * 10**(-9)
        else:
            T_iono = F * (5 * 10**(-9) + AMP * (1 - x**2 / 2 + x**4 / 4))

        return T_iono

    def get_weight(self, sv):
        ''' Fairly daft weighting function where the satellite's
            trustworthiness is just its 'verticalness'; i.e. Sine
            of elevation, on the basis that up is good, horizon is
            bad. '''
        [El, A] = calc_azimuth_elevation(
            self.est_pos, self.eph.get_sat_ECEF(sv, self.get_gps_time(sv)))

        return sin(El)

    def set_est_pos(self, ecef):
        ''' Should be updated each time a solver generates a solution
                    in order to refine the position-dependant corrections (i.e.
                        most of them!).  If the receiver position jumps significantly
                        in a single time step (e.g. initialisation without a good
                        guess) then the solution may have to be repeated after this
                        is updated '''
        self.est_pos = ecef

    def get_est_pos(self):
        return self.est_pos

    def set_pr_correction(self, sv, corr):
        ''' Manually add a pseudorange correction factor for a particular
                    satellite.  Like setting the estimated position, the solution
                        may have to be repeated after this is updated'''
        print(sv, corr)
        self.pr_corr[sv] = corr

    def get_pr_correction(self, sv):
        if sv in self.pr_corr:
            return self.pr_corr[sv]
        else:
            return 0

    def clear_pr_correction(self):
        self.pr_corr = {}

    def get_prange_sv(self, sv):
        ''' Get fully corrected pseudorange for the given sv'''
        clk_off = self._sat_clk_offset(sv) * C  # conv to m
        rel_off = self._sat_rel_offset(sv) * C  # conv to m
        trop_off = self._tropospheric_correction(sv)  # already m
        iono_off = self._ionospheric_correction(sv) * C  # conv to m

        # If we have manually wedged an extra correction in there, use it
        if sv in self.pr_corr:
            manual_off = self.pr_corr[sv]
        else:
            manual_off = 0

        return self._get_prange_sv(sv).prange + clk_off + rel_off + trop_off + iono_off + manual_off

    def lockdown(self):
        '''Stops the complete rangeset or underlying Ephemeris changing
           so a solver can get a good look at it all.  Must be called
           at the beginning of a solution phase '''
        self.cond.acquire()
        self.eph.lockdown()

    def lockdown_wait_new(self):
        '''Waits until a new set of Range data is available and returns
           holding a lock on it, ready for processing'''
        self.cond.acquire()
        self.cond.wait()

        self.eph.lockdown()

    def release(self):
        '''Re-enables changes to the rangeset and Ephemeris.  Must be called
           at the end of a solution phase'''
        self.eph.release()
        self.cond.release()
