'''
Utility functions and constants

Copyright Ben Nizette 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.'''

from numpy import *
from numpy.linalg import norm

from math import *

# The calculations require a specific value of pi
gps_pi = 3.1415926535898

C = 299792458


def ECEF2GPS(pos):
    '''Convert ECEF (x,y,z) to Lat/Long/Alt
       Based on  ECEF2LLA, Copyright Michael Kleder, April 2006 '''
    x = pos[0]
    y = pos[1]
    z = pos[2]

    # WGS84 ellipsoid constants:
    a = 6378137
    e = 8.1819190842622e-2

    # calculations:
    b = sqrt(a**2 * (1 - e**2))
    ep = sqrt((a**2 - b**2) / b**2)
    p = sqrt(x**2 + y**2)
    th = atan2(a * z, b * p)
    lon = atan2(y, x)
    lat = atan((z + ep**2 * b * (sin(th))**3) / (p - e**2 * a * (cos(th))**3))
    N = a / sqrt(1 - e**2 * (sin(lat))**2)
    alt = p / cos(lat) - N

    # correct for numerical instability in altitude near exact poles:
    # (after this correction, error is about 2 millimeters, which is about
    # the same as the numerical precision of the overall function)
    if (abs(x) < 1) & (abs(y) < 1):
        alt = abs(z) - b

    return [lat, lon, alt]


def ECEF2LLA(pos):
    lat, lon, alt = ECEF2GPS(pos)

    return [lon * 180/pi, lat * 180/pi, alt]


def XYZ2ENU(A, Phi, Lambda):
    ''' Convert ECEF coordinates to local East, North, Up
        Based on the code by Moein Mehrtash, Concordia University, 3/21/2008
        Reference:"GPS Theory and application",edited by B.Parkinson,J.Spilker'''
    if not isinstance(A, array.__class__):
        A = array(A)

    XYZ2ENU = array(
        [[-sin(Lambda),             cos(Lambda),            0],
         [-sin(Phi) * cos(Lambda), -sin(Phi) * sin(Lambda), cos(Phi)],
         [cos(Phi) * cos(Lambda),  cos(Phi) * sin(Lambda), sin(Phi)]])

    return dot(XYZ2ENU, A.T)


def calc_azimuth_elevation(rx, sat):
    ''' This Function Compute Azimuth and Elevation of satellite from reciever
        Based on code CopyRight By Moein Mehrtash
        Written by Moein Mehrtash, Concordia University, 3/21/2008'''

    R = array(sat) - array(rx)  # vector from Reciever to Satellite
    GPS = ECEF2GPS(rx)  # Latitude and Longitude of Reciever
    Lat = GPS[0]
    Lon = GPS[1]

    ENU = XYZ2ENU(R, Lat, Lon)
    Elevation = asin(ENU[2] / norm(ENU))
    Azimuth = atan2(ENU[0] / norm(ENU), ENU[1] / norm(ENU))

    return [Elevation, Azimuth]


if __name__ == '__main__':
    '''Half-hearted test cases for the above'''
    ecef = [-4474385.0, 2675686.7, -3663095.7]
    sv = [-2.629576375564667e+007, 2.951067493901677e+006, 3.017037693291128e+006]

    e2g = [round(x, 4) for x in ECEF2GPS(ecef)]
    print(e2g == [-0.6157, 2.6026, 586.5849])

    e2e = [round(x) for x in XYZ2ENU(ecef, pi / 4, pi / 4)]
    print(e2e == [5055864.0, -1690851.0, -3489549.0])

    ae = [round(x, 4) for x in calc_azimuth_elevation(ecef, sv)]
    print(ae == [0.5305, 0.5907])
