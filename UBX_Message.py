
'''
Representations of a UBX class uBlox messages

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import struct

from bitstring import BitArray

msgtypes = {
    '': '',
    'GLL': b'\xf0\x01',
    'RMC': b'\xf0\x04',
    'GSV': b'\xf0\x03',
    'GGA': b'\xf0\x00',
    'GSA': b'\xf0\x02',
    'VTG': b'\xf0\x05',
    'ZDA': b'\xf0\x08',
    'RXM-RAW': b'\x02\x10',
    'AID-EPH': b'\x0b\x31',
    'AID-HUI': b'\x0b\x02',
    'CFG-MSG': b'\x06\x01',
    'CFG-NAV5': b'\x06\x24',
    'CFG-CFG': b'\x06\x09',
    'CFG-RATE': b'\x06\x08',
    'CFG-NAV5': b'\x06\x24',
    'ACK-ACK': b'\x05\x01',
    'ACK-NAK': b'\x05\x00',
    'NAV-SOL': b'\x01\x06'
}

sync = b'\xb5\x62'


def U1(b):
    return b[0]


def U2(b):
    return b[0] + (b[1] << 8)


def U4(b):
    return b[0] + (b[1] << 8) + (b[2] << 16) + (b[3] << 24)


def U8(b):
    return b[0] + (b[1] << 8) + (b[2] << 16) + (b[3] << 24) + (b[4] << 32) + (b[5] << 40) + (b[6] << 48) + (b[7] << 56)


def I1(b):
    u = U1(b)
    if (u > (2**7) - 1):
        return u - (2**8)
    else:
        return u


def I2(b):
    u = U2(b)
    if (u > (2**15) - 1):
        return u - (2**16)
    else:
        return u


def I4(b):
    u = U4(b)
    if (u > (2**31) - 1):
        return u - (2**32)
    else:
        return u


def X1(b):
    return U1(b)


def X2(b):
    return U2(b)


def X4(b):
    return U4(b)


def CH(b):
    return chr(b[0])


def R4(b):
    return BitArray(uint=U4(b), length=32).float


def R8(b):
    return BitArray(uint=U8(b), length=64).float


class Message(object):
    ''' Base class for UBX messages '''
    def __init__(self, command='', payload=''):
        self.sync = sync  # sync bytes
        self.msgid = msgtypes[command]
        self.payload = payload
        self.length = len(self.payload)

    def bytes(self):
        msg = struct.pack('cc', self.sync[0:1], self.sync[1:2])
        msg += struct.pack('cc', self.msgid[0:1], self.msgid[1:2])
        msg += struct.pack('<h', self.length)
        msg += self.payload
        msg += self.checksum(msg[2:])  # slice out the sync bytes
        return msg

    def checksum(self, msg):
        ck_a = 0x00
        ck_b = 0x00
        # b = buffer(msg)
        for i in msg:
            ck_a += i
            ck_a &= 255
            ck_b += ck_a
            ck_b &= 255
        return struct.pack('BB', ck_a, ck_b)

    def binstr(self):
        return bytes(self.bytes())

    def __str__(self):
        return str(self.bytes())

    def __iter__(self):
        return self.bytes().__iter__()


class ConfigureMessage(Message):
    def __init__(self, msg, rate):
        Message.__init__(self, 'CFG-MSG', msgtypes[msg] + rate)

class NAV5Message(Message):
    def __init__(self, **kw):
        mask = 0
        msg = b''
        
        if 'dyn' in kw:
            mask = mask | 1
            msg = msg + kw['dyn']
        else:
            msg = msg + b'\x00'

        msg = msg + bytes(33)

        # TODO: Other message elements.  Can wait until this whole
        # class is moved to use the struct package properly

        Message.__init__(self, 'CFG-NAV5', bytes([mask & 0xFF, mask >> 8]) + msg)

class PollMessage(Message):
    def __init__(self, msg):
        Message.__init__(self, msg, b'')


class RateMessage(Message):
    def __init__(self, rate):
        Message.__init__(self, 'CFG-RATE', bytes([(
            rate & 0xFF), (rate >> 8)]) + b'\x00\x01\x00\x00')
