
'''
Solves a single range set for a single receiver position

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import itertools as itool

import scipy.optimize

from numpy import *
from numpy.linalg import *

from util import C


class UnsolveableException(Exception):
    pass


class Simple_Solver:
    def __init__(self, ident, rangeset, logfile=None):
        self.rangeset = rangeset
        self.ident = ident
        self.last = [0, 0, 0, 0]

        self.logfile = None
        if logfile:
            self.logfile = logfile + "-SOL-" + str(ident) + ".dump"


    def _write_logfile(self, line):
        if self.logfile is None:
            return

        with open(self.logfile, 'a') as f:
            f.write(line)

    def _prepare_data(self, rx):
        # Use only the sats for which we have both eph and pr data
        sats = self.rangeset.get_sats() & self.rangeset.eph.get_sats()

        #print("solver {} eph {} act {}".format(self.ident, self.rangeset.eph.get_sats(), self.rangeset.get_active_sats()))

        num_sv = len(sats)
        data = []
        for sat in sats:
            gps_time = self.rangeset.get_gps_time(sat)

            prange = array(self.rangeset.get_prange_sv(sat))
            sat_pos = array(self.rangeset.eph.get_sat_ECEF_at(
                sat, gps_time, prange))

            data.append((sat, sat_pos, prange))

        return num_sv, data

    def _calc_resid(self, rx, data):
        ''' Calculates a tuple of residual distance errors from each
            satellite given a solved position and a set of PRs'''

        resid = []
        rp = rx[0:3]
        b = rx[3]

        for sv, sat_pos, pr in data:
            r = norm(rp - sat_pos) - pr + C * b
            resid.append((sv, r))

        return resid

    def _lsq_residual(self, p, data, rough):
        rx = p[0:3]
        b = p[3]

        resid = []
        for d in data:
            sv, sv_pos, pr = d

            dist = norm(sv_pos - rx)

            res = dist - pr + C * b

            # If we've been asked for a rough solution it's because we have
            # no faith in our initial condition.  This doesn't affect most
            # corrections that much, but the elevation weighting is more
            # sensitive and if we use it while, e.g., at the origin, our
            # first several fixes are going to be rubbish (if they converge
            # at all!)
            if not rough:
                res = res * self.rangeset.get_weight(sv)

            resid.append(res)

        return resid

    def _step(self, rough):
        rx = self.last[:]
        self.rangeset.set_est_pos(rx[0:3])

        num_sv, data = self._prepare_data(rx)

        if num_sv < 4:
            self._write_logfile("STEP UNSOLVEABLE {}\n".format(num_sv))
            raise UnsolveableException("Not enough satellites ({})".format(num_sv))

        # If we've been asked for a rough run, increase the acceptable error.
        # This option also changes the objective function (see above)
        if rough:
            xtol = 1.0E-4
        else:
            xtol = 1.0E-8

        rx_n, ier = scipy.optimize.leastsq(
            self._lsq_residual, rx, args=(data, rough), xtol=xtol)

        self.last = rx_n[:]
        self._write_logfile("STEP SOLUN {}\n".format(rx_n))

        self.resid = self._calc_resid(rx_n, data)
        self._write_logfile("STEP RESID {}\n".format(self.resid))

        return rx_n

    def solve(self):
        # Stops (our view of) ranges and ephemerides changing while
        # we're playing with them.
        self.rangeset.lockdown_wait_new()

        try:
            # If we haven't got a valid last position, do a dummy
            # run to get us in the vicinity before we go and solve
            # for the final position
            if allclose(self.last, zeros_like(self.last)):
                self._step(True)

            rx_n = self._step(False)
        finally:
            self.rangeset.release()

        return rx_n[0:3]
