
'''
Representations and computations regarding the satellite constellation
and Ephemerides

Copyright Ben Nizette 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.'''

import threading
import time

from collections import namedtuple

import scipy.optimize

import bitstring
from bitstring import BitArray

from math import sin, cos, atan2, sqrt
from util import gps_pi, C


def sl(a, u, l):
    return a[len(a) - u - 1: len(a) - l]


def sfbits(sv, sf, dw, u, l):
    word = (sf - 1) * 8 + (dw - 3)
    return BitArray(uint=(sv[word][32 - u - 1:32 - l]).uint, length=32)

class Ephemeride(namedtuple('Ephemeride','rootA toc deltaN M0 omega Cus Cuc Crs Crc Cis Cic i0 idot omega0 omegadot e af2 af1 af0 iode toe Tgd')):
    pass

class Ephemeris:
    def __init__(self):
        self.ephemeris = {}
        self.eph_lock = threading.Lock()
        self.valid = False

    def load_eph_entry(self, sv, data):
        s = [BitArray(uint=b, length=32) for b in data]

        # Extract bitfields
        rootA = (sfbits(s, 2, 8, 7, 0) << 24) | sfbits(s, 2, 9, 23, 0)
        toc = sfbits(s, 1, 8, 15, 0)
        deltaN = sfbits(s, 2, 4, 23, 8)
        M0 = (sfbits(s, 2, 4, 7, 0) << 24) | sfbits(s, 2, 5, 23, 0)
        omega = (sfbits(s, 3, 7, 7, 0) << 24) | sfbits(s, 3, 8, 23, 0)
        Cus = sfbits(s, 2, 8, 23, 8)
        Cuc = sfbits(s, 2, 6, 23, 8)
        Crs = sfbits(s, 2, 3, 15, 0)
        Crc = sfbits(s, 3, 7, 23, 8)
        Cis = sfbits(s, 3, 5, 23, 8)
        Cic = sfbits(s, 3, 3, 23, 8)
        i0 = (sfbits(s, 3, 5, 7, 0) << 24) | sfbits(s, 3, 6, 23, 0)
        idot = sfbits(s, 3, 10, 15, 2)
        omega0 = (sfbits(s, 3, 3, 7, 0) << 24) | sfbits(s, 3, 4, 23, 0)
        omegadot = sfbits(s, 3, 9, 23, 0)
        e = (sfbits(s, 2, 6, 7, 0) << 24) | sfbits(s, 2, 7, 23, 0)
        af2 = sfbits(s, 1, 9, 23, 16)
        af1 = sfbits(s, 1, 9, 15, 0)
        af0 = sfbits(s, 1, 10, 23, 2)
        iode = sfbits(s, 2, 3, 23, 16)
        toe = sfbits(s, 2, 10, 23, 8)
        Tgd = sfbits(s, 1, 7, 7, 0)

        # Apply scaling factors and types
        self.eph_lock.acquire()
        self.ephemeris[sv] = Ephemeride(
                rootA = rootA.uint * (2**(-19)),
                toc = sl(toc, 15, 0).uint * (2**4),
                deltaN = sl(deltaN, 15, 0).int * (2**(-43)),
                M0 = M0.int * (2**(-31)),
                omega = omega.int * (2**(-31)),
                Cus = sl(Cus, 15, 0).int * (2**(-29)),
                Cuc = sl(Cuc, 15, 0).int * (2**(-29)),
                Crs = sl(Crs, 15, 0).int * (2**(-5)),
                Crc = sl(Crc, 15, 0).int * (2**(-5)),
                Cis = sl(Cis, 15, 0).int * (2**(-29)),
                Cic = sl(Cic, 15, 0).int * (2**(-29)),
                i0 = i0.int * (2**(-31)),
                idot = sl(idot, 13, 0).int * (2**(-43)),
                omega0 = omega0.int * (2**(-31)),
                omegadot = sl(omegadot, 23, 0).int * (2**(-43)),
                e = e.uint * (2**(-33)),
                af2 = sl(af2, 7, 0).int * (2**(-55)),
                af1 = sl(af1, 15, 0).int * (2**(-43)),
                af0 = sl(af0, 21, 0).int * (2**(-31)),
                iode = sl(iode, 7, 0).uint,
                toe = sl(toe, 15, 0).uint * (2**(4)),
                Tgd = sl(Tgd, 7, 0).int * (2**(-31))
            )

        # Since we don't know what a full Ephemeris is, we assert validity
        # at the first entry on the basis that this is going to be close
        # in time to the Ephemerides for all current satellites coming through,
        # and that the rest of the code has to be able to deal with partial
        # data anyway
        self.valid = True

        #if len(self.ephemeris) < 4:
        #    print("Ephemeris short ({})".format(len(self.ephemeris)))
        
        self.eph_lock.release()

    def get_sats(self):
        return set(self.ephemeris.keys())

    def get_sat_ephemeris(self, sv):
        return self.ephemeris[sv]

    def calc_sat_orbit(self, sv, gps_time):
        # This is pretty darn fast (~40ms on my desktop) but if we're hammering
        # it good, might be worth setting up a results cache here
        if sv not in self.ephemeris:
            return None

        # The following is based on Moein Mehrtash's MATLAB GPS Toolbox Code
        # Copyright By Moein Mehrtash
        # Written by Moein Mehrtash, Concordia University, 3/28/2008
        # Email: moeinmehrtash@yahoo.com

        # Constants
        Mu = 3.986005 * (10**14)
        Omega_dote = 7.2921151467 * (10**(-5))

        # Load Data
        A = self.ephemeris[sv].rootA ** 2
        t = gps_time
        toe = self.ephemeris[sv].toe
        Delta_n = self.ephemeris[sv].deltaN
        M0 = self.ephemeris[sv].M0
        omega = self.ephemeris[sv].omega
        C_us = self.ephemeris[sv].Cus
        C_uc = self.ephemeris[sv].Cuc
        C_rs = self.ephemeris[sv].Crs
        C_rc = self.ephemeris[sv].Crc
        C_is = self.ephemeris[sv].Cis
        C_ic = self.ephemeris[sv].Cic
        i0 = self.ephemeris[sv].i0
        IDOT = self.ephemeris[sv].idot
        Omega0 = self.ephemeris[sv].omega0
        Omegadot = self.ephemeris[sv].omegadot
        e = self.ephemeris[sv].e

        # Time from reference epoch
        tk = t - toe

        # Compute corrected mean motion
        n0 = sqrt(Mu / (A**3))
        n = (n0 / gps_pi) + Delta_n
        Mk = M0 + n * tk

        # Solve Kepler Eq. with Initial value Ek0 = Mk * pi
        Ek = scipy.optimize.root(
			lambda x: x - e*sin(x) - (Mk*gps_pi), Mk*gps_pi).x[0]

        # Compute True Anomaly as a Function of Eccentric Anomaly
        Sin_nuk = sqrt(1 - e**2) * sin(Ek) / (1 - e*cos(Ek))
        Cos_nuk = (cos(Ek) - e) / (1 - e*cos(Ek))
        nuk = atan2(Sin_nuk, Cos_nuk)

        if nuk < 0:
            nuk = nuk + 2*gps_pi

        # Compute Argument of Latitude and Correction
        Phik = nuk + omega*gps_pi

        # Compute Argument of Latitude, Radius and Inclination Correction
        Delta_uk = C_us * sin(2*Phik) + C_uc * cos(2*Phik)
        Delta_rk = C_rs * sin(2*Phik) + C_rc * cos(2*Phik)
        Delta_ik = C_is * sin(2*Phik) + C_ic * cos(2*Phik)

        # Compute Corrected Value of Latitude,Radius,Inclination and Ascending
        # node

        uk = Phik + Delta_uk
        # Latitude
        rk = A * (1 - e*cos(Ek)) + Delta_rk  # Radius
        ik = i0 * gps_pi + Delta_ik + IDOT * tk * gps_pi  # Inclination
        Omegak = Omega0 * gps_pi + (Omegadot*gps_pi - Omega_dote) * tk - Omega_dote * toe

        # Compute satellite vehicle position
        # Satellite position in orbital plane
        x_Perk = rk * cos(uk)
        y_Perk = rk * sin(uk)

        # Satellite Position in ECEF
        xk = x_Perk * cos(Omegak) - y_Perk * cos(ik) * sin(Omegak)
        yk = x_Perk * sin(Omegak) + y_Perk * cos(ik) * cos(Omegak)
        zk = y_Perk * sin(ik)

        return [xk, yk, zk, Ek]

    def get_sat_ECEF(self, sv, gps_time):
        return self.calc_sat_orbit(sv, gps_time)[0:3]  # Slice off the eccentricity datam

    def get_sat_ECEF_at(self, sv, gps_time, pr):
        ''' Returns the satellite position in the ECEF frame at a given system
            time and pseudorange.  The pr measurement allows us to compensate
            for ToF effects (Sagnac etc.) '''
        tof = pr / C

        [X, Y, Z] = self.get_sat_ECEF(sv, gps_time - tof)

        # Correct for the rotation of the earth during flight.  Note that
        # the rotation is about the ECEF Z-axis by definition so there is
        # no correction required about Z
        # WGS-84 earth rotation rate
        We = 7.292115E-5

        a = tof * We
        X = X * cos(a) + Y * sin(a)
        Y = -X * sin(a) + Y * cos(a)

        return [X, Y, Z]

    def lockdown(self):
        self.eph_lock.acquire()

    def release(self):
        self.eph_lock.release()

    def __str__(self):
        return str(self.ephemeris)


if __name__ == '__main__':
    # Subframe input data and verified ephemerides
    eph = [
        0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000da, 0x5c42cc, 0x000011, 0x37e84c, 0x5cfa00, 0x36b64a, 0x58bf33, 0xfade06,
        0x4747d5, 0x159aa1, 0x0d21c4, 0x42cc00, 0xff74e7, 0x3807f7, 0x004e26, 0x40e5ae, 0x15cc95, 0x02d29e, 0xffa574, 0x5ce0f8]
    ans = [
        -4.8000000000e+01, 1.5922978491e-09, 5.8083333960e-01, -2.4475157261e-06, 1.2262577773e-02, 1.0300427675e-05, 5.1536414871e+03, 2.7360000000e+05, -2.6077032089e-07, -1.9360256614e-01,
        1.4528632164e-07, 2.9885550495e-01, 1.7437500000e+02, -8.3585135732e-01, -2.6352608984e-09, -2.2578205972e-10, -1.7695128918e-08, 2.7360000000e+05, 0.0000000000e+00, 1.9326762413e-12, 4.2653968558e-04]

    # Verified sat location and eccentricity for the above ephemerides
    t = 266879.600
    sat_loc = [-1.607385605535492e+007, -5.297986087327972e+005, -2.086748284282296e+007]
    Ek = 8.537357403566304e-001

    dut = Ephemeris()
    dut.load_eph_entry(2, eph)

    sat = dut.get_sat_ephemeris(2)

    # This looks a queer way to do this (and it is), but this format string was the
    # one used to generate the verified ephemerides from another program, we generate
    # in the same way to avoid format problems
    entry = [float(
        x) for x in '{0[Crs]:.10e} {0[deltaN]:.10e} {0[M0]:.10e} {0[Cuc]:.10e} {0[e]:.10e} {0[Cus]:.10e} {0[rootA]:.10e} {0[toe]:.10e} {0[Cic]:.10e} {0[omega0]:.10e} {0[Cis]:.10e} {0[i0]:.10e} {0[Crc]:.10e} {0[omega]:.10e} {0[omegadot]:.10e} {0[idot]:.10e} {0[Tgd]:.10e} {0[toc]:.10e} {0[af2]:.10e} {0[af1]:.10e} {0[af0]:.10e}'.format(sat).split()]

    if len(entry) != len(ans):
        print("Wrong length")
        quit()

    for i in range(1, len(entry)):
        if (entry[i] != ans[i]):
            print("Entry {} wrong {} != {}".format(i, entry[i], ans[i]))
            quit()

    [x, y, z, E] = dut.calc_sat_orbit(2, t)

    # Round to centimetres, we don't care beyond that
    sat_loc = [round(x, 2) for x in sat_loc]
    x = round(x, 2)
    y = round(y, 2)
    z = round(z, 2)

    if (x != sat_loc[0] or y != sat_loc[1] or z != sat_loc[2]):
        print("{} {} {} != {} {} {}".format(
            x, y, z, sat_loc[0], sat_loc[1], sat_loc[2]))
        quit()

    E = round(E, 8)
    Ek = round(Ek, 8)

    if (E != Ek):
        print("{} != {}".format(E, Ek))
        quit()

    print("Pass")
