
'''
Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import argparse
import Ephemeris_Model
import Range_Model
import Simple_Solver
import util

import math

import kml_output as kml
from numpy import array

from time import sleep

from UBX_Module import UBX_Module
from Fake_Module import Fake_Module

# Use prerecorded data
fake_data = False
fakes = 1

parser = argparse.ArgumentParser()

parser.add_argument('--port', action='append')
args = parser.parse_args()

if args.port == None:
    args.port = ["COM8"]

eph_model = Ephemeris_Model.Ephemeris()

modules = []

if fake_data:
    for ident in range(0, fakes):
        loc_model = Range_Model.Corrected_Rangeset(eph_model)
        modules.append(Fake_Module(ident, eph_model, loc_model))
else:
    ident = 0
    for p in args.port:
        loc_model = Range_Model.Corrected_Rangeset(eph_model)
        modules.append(UBX_Module(p, ident, eph_model, loc_model))
        ident += 1

solver1 = Simple_Solver.Simple_Solver(modules[0].loc)

sink1 = kml.KML_Output("warmup.kml")
sink2 = kml.KML_Output("full.kml")

sink1.prepare()
sink2.prepare()

for i in range(1, 1000):
    try:
        s1 = solver1.solve()
    except Simple_Solver.UnsolveableException:
        print("<unsolveable>")
    except (KeyboardInterrupt, SystemExit):
        sink1.finalise()
        raise
    else:
        sink1.register_point(1, s1)
        print(array(util.ECEF2GPS(s1)) * array(
            [180.0/math.pi, 180.0/math.pi, 1]))

sink1.finalise()
'''
for i in range(1,1000):
    try:
        s1 = solver1.solve()
    except Simple_Solver.UnsolveableException:
        print("<unsolveable>")
    except (KeyboardInterrupt, SystemExit):
        sink2.finalise()
        raise
    else:
        sink2.register_point(1, s1)
        print(array(util.ECEF2GPS(s1)) * array([180.0/math.pi, 180.0/math.pi, 1]))

sink2.finalise()
'''
for m in modules:
    m.stop()
