
from Simple_Solver import Simple_Solver
import Range_Model

class Compound_Solver:
    def __init__(self, rangesets, logfile=None):
        self.solvers = []

        for i, rset in enumerate(rangesets):
            s = Simple_Solver(i, rset, logfile=logfile)
            self.solvers.append(s)

        self.logfile = None
        if logfile:
            self.logfile = logfile + "COMP.dump"


    def _write_logfile(self, line):
        if self.logfile is None:
            return

        with open(self.logfile, 'a') as f:
            f.write(line)


    def solve(self):
        resids = []
        sols = []
        max_sv = 30
        alpha = 0.01
        
        # Each solve will wait for new ranges from the backing
        # rangeset before returning
        for s in self.solvers:
            sols.append((s.ident, s.solve()))
            resids.append(s.resid)

        self._write_logfile("COMP SOLS {}\n".format(sols))
        self._write_logfile("COMP RESI {}\n".format(resids))

        # look at the first 30 SVs (more than enough for now)
        # TODO: iterate only over SVs that actually exist
        c = {}

        for solution in resids:
            for sv, res in solution:

                if sv not in c:
                    c[sv] = {'t':0, 'n':0}
                c[sv]['t'] = c[sv]['t'] + res
                c[sv]['n'] = c[sv]['n'] + 1

        self._write_logfile("COMP EREST {}\n".format(c))

        for s in self.solvers:
            s.rangeset.lockdown()
            
            for sv in range(0, max_sv):
                if sv not in c:
                    continue
                if c[sv]['n'] > 0:
                    cval = ((1-alpha) * s.rangeset.get_pr_correction(sv)) + \
                        (alpha * c[sv]['t'] / c[sv]['n'])
                           
                    s.rangeset.set_pr_correction(sv, -cval)

            s.rangeset.release()

        return sols
    
