

'''
Standalone parsing of Ephemeris and Range data in to a form that can be ^C^V in to MATLAB

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import time
from bitstring import BitArray

ephdata = """SV	HOW	SF1D00	SF1D01	SF1D02	SF1D03	SF1D04	SF1D05	SF1D06	SF1D07	SF2D00	SF2D01	SF2D02	SF2D03	SF2D04	SF2D05	SF2D06	SF2D07	SF3D00	SF3D01	SF3D02	SF3D03	SF3D04	SF3D05	SF3D06	SF3D07
1
2	345600	b1c000	000000	000000	000000	0000da	5b5460	000011	37ecd0	5bf9fa	393ef6	34635c	fabb06	479654	1181a1	0d131e	546000	0056e7	31d47f	004326	407da5	1a0995	0b989c	ffa262	5be308
3
4	345600	b1c000	000000	000000	000000	0000f3	385460	00005f	187d98	38f96d	36d079	add97c	fa3805	47d4af	125aa1	0e326b	546000	0030e7	dde7da	ffc626	369784	189a27	83982e	ffa757	38e320
5
6
7
8
9	345486	b1c000	000000	000000	000000	0000f4	4a5459	000017	1e8fac	4af200	2e49cd	2c5fd1	f48608	d8d508	1075a1	066064	545900	ff8667	daffb1	00a028	19ed7e	1ee543	09b46e	ffa70d	4af19c
10
11
12	345600	b1c000	000000	000000	000000	0000e6	105460	000012	10bd08	10ffaf	2af9b2	05c39e	ffd702	4b7404	18c5a1	0cc277	546000	003193	ea1c53	ffc928	157c39	146708	59dcdc	ffacc0	1014a0
13
14
15
16
17	345600	b1c000	000000	000000	000000	0000ea	2d5460	00ffdd	0413e8	2d023f	36972d	6e8f06	021104	3fb09a	09b3a1	0ccd21	546000	ffc5be	ed20cd	002127	4d7649	2500a6	715718	ffa3da	2d0070
18
19
20
21
22
23
24	345600	b1c100	000000	000000	000000	000007	5a5460	00fffa	ff6fd8	5af11b	32fedb	831b52	f31000	736417	0e02a1	0d86dc	546000	002067	af0ea9	fff327	14210f	1f470d	7ca03f	ffa5f2	5af194
25
26
27
28	345600	b1c000	000000	000000	000000	0000e9	0e5460	00001e	209d54	0e006b	2a814e	06b1c4	ffde09	827827	19e3a1	0d264f	546000	003194	eb24d5	ff2828	0c6432	12d7b7	cc0d47	ffae94	0e14ac
29
30
31
32
"""

prdata = """SV	QI	SNR	Lock	Carrier Phase	Pseudo Range	Doppler
2	4	29	3	-13603779.22	20280718.51	2197.7
12	4	27	3	-11793610.42	21904467.53	2730.6
4	7	36	0	-4100131.60	20870758.72	-398.0
24	4	13	3	-3447208.36	20893256.01	-288.6
17	4	21	3	9323334.29	23083069.23	-2579.6
9	4	21	3	10363202.51	23103720.85	-3149.7
"""

s = {}
pr = {}


def sl(a, u, l):
    return a[len(a) - u - 1: len(a) - l]


def sfbits(sv, sf, dw, u, l):
    word = (sf - 1) * 8 + (dw - 3)
    return BitArray(uint=(sv[word][32 - u - 1:32 - l]).uint, length=32)

for l in ephdata.splitlines(False):
    i = l.split()
    if len(i) < 26 or i[0] == 'SV':
        continue

    s[i[0]] = [BitArray(int=int(b, 16), length=32) for b in i[2:]]

for l in prdata.splitlines(False):
    i = l.split()
    if len(i) < 7 or i[0] == 'SV':
        continue

    pr[i[0]] = i[5]

sat = {}
for sv in s:
    sat[sv] = {}
    sat[sv]['rootA'] = (sfbits(s[sv], 2, 8, 7, 0) << 24) | sfbits(s[sv], 2, 9, 23, 0)
    sat[sv]['toc'] = sfbits(s[sv], 1, 8, 15, 0)
    sat[sv]['deltaN'] = sfbits(s[sv], 2, 4, 23, 8)
    sat[sv]['M0'] = (sfbits(s[sv], 2, 4, 7, 0) << 24) | sfbits(s[sv], 2, 5, 23, 0)
    sat[sv]['omega'] = (sfbits(s[sv], 3, 7, 7, 0) << 24) | sfbits(s[sv], 3, 8, 23, 0)
    sat[sv]['Cus'] = sfbits(s[sv], 2, 8, 23, 8)
    sat[sv]['Cuc'] = sfbits(s[sv], 2, 6, 23, 8)
    sat[sv]['Crs'] = sfbits(s[sv], 2, 3, 15, 0)
    sat[sv]['Crc'] = sfbits(s[sv], 3, 7, 23, 8)
    sat[sv]['Cis'] = sfbits(s[sv], 3, 5, 23, 8)
    sat[sv]['Cic'] = sfbits(s[sv], 3, 3, 23, 8)
    sat[sv]['i0'] = (sfbits(s[sv], 3, 5, 7, 0) << 24) | sfbits(s[sv], 3, 6, 23, 0)
    sat[sv]['idot'] = sfbits(s[sv], 3, 10, 15, 2)
    sat[sv]['omega0'] = (sfbits(s[sv], 3, 3, 7, 0) << 24) | sfbits(s[sv], 3, 4, 23, 0)
    sat[sv]['omegadot'] = sfbits(s[sv], 3, 9, 23, 0)
    sat[sv]['e'] = (sfbits(s[sv], 2, 6, 7, 0) << 24) | sfbits(s[sv], 2, 7, 23, 0)
    sat[sv]['af2'] = sfbits(s[sv], 1, 9, 23, 16)
    sat[sv]['af1'] = sfbits(s[sv], 1, 9, 15, 0)
    sat[sv]['af0'] = sfbits(s[sv], 1, 10, 23, 2)
    sat[sv]['iode'] = sfbits(s[sv], 2, 3, 23, 16)
    sat[sv]['toe'] = sfbits(s[sv], 2, 10, 23, 8)
    sat[sv]['Tgd'] = sfbits(s[sv], 1, 7, 7, 0)

for sv in sat:
    sat[sv]['rootA'] = sat[sv]['rootA'].uint * (2**(-19))
    sat[sv]['toc'] = sl(sat[sv]['toc'], 15, 0).uint * (2**4)
    sat[sv]['deltaN'] = sl(sat[sv]['deltaN'], 15, 0).int * (2**(-43))
    sat[sv]['M0'] = sat[sv]['M0'].int * (2**(-31))
    sat[sv]['omega'] = sat[sv]['omega'].int * (2**(-31))
    sat[sv]['Cus'] = sl(sat[sv]['Cus'], 15, 0).int * (2**(-29))
    sat[sv]['Cuc'] = sl(sat[sv]['Cuc'], 15, 0).int * (2**(-29))
    sat[sv]['Crs'] = sl(sat[sv]['Crs'], 15, 0).int * (2**(-5))
    sat[sv]['Crc'] = sl(sat[sv]['Crc'], 15, 0).int * (2**(-5))
    sat[sv]['Cis'] = sl(sat[sv]['Cis'], 15, 0).int * (2**(-29))
    sat[sv]['Cic'] = sl(sat[sv]['Cic'], 15, 0).int * (2**(-29))
    sat[sv]['i0'] = sat[sv]['i0'].int * (2**(-31))
    sat[sv]['idot'] = sl(sat[sv]['idot'], 13, 0).int * (2**(-43))
    sat[sv]['omega0'] = sat[sv]['omega0'].int * (2**(-31))
    sat[sv]['omegadot'] = sl(sat[sv]['omegadot'], 23, 0).int * (2**(-43))
    sat[sv]['e'] = sat[sv]['e'].uint * (2**(-33))
    sat[sv]['af2'] = sl(sat[sv]['af2'], 7, 0).int * (2**(-55))
    sat[sv]['af1'] = sl(sat[sv]['af1'], 15, 0).int * (2**(-43))
    sat[sv]['af0'] = sl(sat[sv]['af0'], 21, 0).int * (2**(-31))
    sat[sv]['iode'] = sl(sat[sv]['iode'], 7, 0).uint
    sat[sv]['toe'] = sl(sat[sv]['toe'], 15, 0).uint * (2**(4))
    sat[sv]['Tgd'] = sl(sat[sv]['Tgd'], 7, 0).int * (2**(-31))

print("eph=[")
for sv in sat:
    if sv in pr:
        print('{0[Crs]:.20e}, {0[deltaN]:.20e}, {0[M0]:.20e}, {0[Cuc]:.20e}, {0[e]:.20e}, {0[Cus]:.20e}, {0[rootA]:.20e}, {0[toe]:.20e}, {0[Cic]:.20e}, {0[omega0]:.20e}, {0[Cis]:.20e}, {0[i0]:.20e}, {0[Crc]:.20e}, {0[omega]:.20e}, {0[omegadot]:.20e}, {0[idot]:.20e}, {0[Tgd]:.20e}, {0[toc]:.20e}, {0[af2]:.20e}, {0[af1]:.20e}, {0[af0]:.20e};'.format(
            sat[sv]))

print("]'")

print("pr=[")
for sv in sat:
    if sv in pr:
        print(pr[sv])

print("]'")
