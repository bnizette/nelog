

'''
Fake module for testing.  Provides a fixed Ephemeris and a single data point

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''


ephemeris = [
[2, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000da, 0x5b5460, 0x000011, 0x37ecd0, 0x5bf9fa, 0x393ef6, 0x34635c, 0xfabb06,
    0x479654, 0x1181a1, 0x0d131e, 0x546000, 0x0056e7, 0x31d47f, 0x004326, 0x407da5, 0x1a0995, 0x0b989c, 0xffa262, 0x5be308],
[4, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000f3, 0x385460, 0x00005f, 0x187d98, 0x38f96d, 0x36d079, 0xadd97c, 0xfa3805,
    0x47d4af, 0x125aa1, 0x0e326b, 0x546000, 0x0030e7, 0xdde7da, 0xffc626, 0x369784, 0x189a27, 0x83982e, 0xffa757, 0x38e320],
[9, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000f4, 0x4a5459, 0x000017, 0x1e8fac, 0x4af200, 0x2e49cd, 0x2c5fd1, 0xf48608,
    0xd8d508, 0x1075a1, 0x066064, 0x545900, 0xff8667, 0xdaffb1, 0x00a028, 0x19ed7e, 0x1ee543, 0x09b46e, 0xffa70d, 0x4af19c],
[12, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000e6, 0x105460, 0x000012, 0x10bd08, 0x10ffaf, 0x2af9b2, 0x05c39e, 0xffd702,
    0x4b7404, 0x18c5a1, 0x0cc277, 0x546000, 0x003193, 0xea1c53, 0xffc928, 0x157c39, 0x146708, 0x59dcdc, 0xffacc0, 0x1014a0],
[17, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000ea, 0x2d5460, 0x00ffdd, 0x0413e8, 0x2d023f, 0x36972d, 0x6e8f06, 0x021104,
    0x3fb09a, 0x09b3a1, 0x0ccd21, 0x546000, 0xffc5be, 0xed20cd, 0x002127, 0x4d7649, 0x2500a6, 0x715718, 0xffa3da, 0x2d0070],
[24, 0xb1c100, 0x000000, 0x000000, 0x000000, 0x000007, 0x5a5460, 0x00fffa, 0xff6fd8, 0x5af11b, 0x32fedb, 0x831b52, 0xf31000,
    0x736417, 0x0e02a1, 0x0d86dc, 0x546000, 0x002067, 0xaf0ea9, 0xfff327, 0x14210f, 0x1f470d, 0x7ca03f, 0xffa5f2, 0x5af194],
[28, 0xb1c000, 0x000000, 0x000000, 0x000000, 0x0000e9, 0x0e5460, 0x00001e, 0x209d54, 0x0e006b, 0x2a814e, 0x06b1c4, 0xffde09,
    0x827827, 0x19e3a1, 0x0d264f, 0x546000, 0x003194, 0xeb24d5, 0xff2828, 0x0c6432, 0x12d7b7, 0xcc0d47, 0xffae94, 0x0e14ac]
]

gps_time = 345697
ranges = [
    [24, 20893256.0100000],
    [12, 21904467.5300000],
    [17, 23083069.2300000],
    [2, 20280718.5100000],
    [4, 20870758.7200000],
    [9, 23103720.8500000]
]


iono = [2.60770000000000e-08, 1.49012000000000e-08, -1.19209000000000e-07,
        -5.96046000000000e-08, 129024, 16384, -262144, 327680]


class Fake_Module:
    ''' Provides a fixed Ephemeris, Ionospheric corrections and range.
                Only emits a single set of raw data. '''
    def __init__(self, ident, eph, loc):
        self.eph = eph
        self.loc = loc
        for e in ephemeris:
            eph.load_eph_entry(e[0], e[1:])

        for pr in ranges:
            # TODO: Load some fake Doppler, Phase
            loc.load_raw_entry(pr[0], gps_time, pr[1], 0, 0)

        loc.load_ionospheric_entry(iono)

    def stop(self):
        pass
