
'''
Generate a KML path from point solutions

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

from lxml import etree
from pykml.factory import KML_ElementMaker as KML
from pykml.factory import GX_ElementMaker as GX

import util


class KML_Output:
    def __init__(self, fname):
        self.fname = fname
        self.points = {}

    def prepare(self):
        pass

    def finalise(self):
        doc = KML.kml()
        fld = KML.Folder()

        doc.append(fld)

        for ident in self.points:
            pointStr = '\n'.join(['{0:.10f},{1:.10f},{2:.10f}'.format(
                *p) for p in self.points[ident]])
            p = KML.Placemark(
                KML.name(str(ident)),
                KML.LineString(
                    KML.extrude('1'),
                    KML.tessellate('1'),
                    GX.altitudeMode('absolute'),
                    KML.coordinates(pointStr)
                ))
            fld.append(p)

        with open(self.fname, 'w') as f:
            f.write(etree.tostring(doc, pretty_print=True).decode("UTF-8"))

    def register_point(self, ident, ecef):
        if not ident in self.points:
            self.points[ident] = []

        self.points[ident].append(util.ECEF2LLA(ecef))
