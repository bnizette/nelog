
'''
Top-level script to manipulate multiple receivers

Copyright Ben Nizette 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.'''

import sys
import argparse
import threading
import datetime

import Ephemeris_Model
import Range_Model

from compound_solver import Compound_Solver
from Simple_Solver import UnsolveableException

import null_solver

import kml_output as kml
import statistic_output as stat_out

from UBX_Module import UBX_Module


parser = argparse.ArgumentParser()

parser.add_argument('--port', action='append',
                    help="Serial port(s) to which the receiver(s) are connected")
parser.add_argument('--kml',
                    help="Write points to a KML file of given filename")
parser.add_argument('--iterations', default=100, type=int,
                    help="Number of iterations to run for.  Includes unsuccessful solution runs")
parser.add_argument('--stats',
                    help="Write statistics to the given file or '-' for stdout")
parser.add_argument('--log-all', action='store_true',
                    help="Write verbose debugging to a set of log files")
args = parser.parse_args()

modules = []
rsets = []

eph = Ephemeris_Model.Ephemeris()

for i, port in enumerate(args.port):
    if args.log_all:
        lfile = datetime.datetime.now().strftime("%y%m%d-%H%M") + "-" + str(i)
    else:
        lfile = None

    loc_model = Range_Model.Corrected_Rangeset(eph, 0, logfile=lfile)
    module = UBX_Module(port, i, loc_model, logfile=lfile)

    modules.append(module)
    rsets.append(loc_model)

sinks = []

if args.kml is not None:
    kml_sink = kml.KML_Output(args.kml)
    sinks.append(kml_sink)

if args.stats is not None:
    stat_sink = stat_out.Stat_Output(args.stats)
    sinks.append(stat_sink)

for sink in sinks:
    sink.prepare()

solver = Compound_Solver(rsets, datetime.datetime.now().strftime("%y%m%d-%H%M") + "-")

for i in range(0, args.iterations):
    print("iteration {}".format(i))
    try:
        sols = solver.solve()
    except UnsolveableException:
        pass
    else:
        for ident, sol in sols:
            for sink in sinks:
                sink.register_point(ident, sol)

for sink in sinks:
    sink.finalise()

for module in modules:
    module.stop()


