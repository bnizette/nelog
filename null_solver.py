
import time

class Null_Solver:
    def __init__(self, ident, rangeset):
        self.rangeset = rangeset
        self.ident = ident

    def solve(self):
        time.sleep(0.2)
        return self.rangeset.mod_solution
