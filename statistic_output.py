
'''
Generate inter-point and inter-solution statistics

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import util
import threading

import numpy as np
import scipy.stats as ss

from math import pi

class Stat_Output:
    def __init__(self, fname):
        self.fname = fname

        if fname == '-':
            self.stdout = True
        else:
            self.stdout = False
            
        self.points = {}
        self.point_lock = threading.Lock()

    def prepare(self):
        pass

    def _gen_stats(self):
        ret = {}
        
        for ident in self.points:
            a = np.array(self.points[ident])

            ret[ident] = {}
            ret[ident]['AVG ECEF'] = np.mean(a, axis=0)

            # Generates a trimmed data set where any point more than 1km
            # from the global mean is discarded
            th = ret[ident]['AVG ECEF'] + 1000
            tl = ret[ident]['AVG ECEF'] - 1000

            tdata = [p for p in a.tolist()
                     if tl[0] < p[0] < th[0] and tl[1] < p[1] < th[1] and tl[2] < p[2] < th[2]]
            tdata = np.array(tdata)
            
            ret[ident]['N'] = len(a)
            ret[ident]['TN'] = len(tdata)

            ret[ident]['TAVG ECEF'] = np.mean(tdata, axis=0)

            lat, lon, alt = util.ECEF2GPS(ret[ident]['AVG ECEF'])
            
            ret[ident]['AVG LLA'] = ([lat, lon, alt] *
                                     np.array([180/pi,180/pi,1]))

            ret[ident]['STD ECEF'] = np.std(a, axis=0)
            ret[ident]['TSTD ECEF'] = np.std(tdata, axis=0)

            # Positive STDs in ECEF might still be negative numbers in ENU
            # (e.g. any positive Z will be negative U in southern hemisphere)
            ret[ident]['STD ENU'] = [np.abs(x) for x in
                                     util.XYZ2ENU(ret[ident]['STD ECEF'], lat, lon)]

            ret[ident]['TSTD ENU'] = [np.abs(x) for x in
                                     util.XYZ2ENU(ret[ident]['TSTD ECEF'], lat, lon)]

            # Check, is this actually RMS..?
            ret[ident]['STD RMS'] = np.linalg.norm(ret[ident]['STD ECEF'])
            ret[ident]['TSTD RMS'] = np.linalg.norm(ret[ident]['TSTD ECEF'])
            
        return ret

    def _write_stats(self, f, stats):
        for ident in stats:
            f.write('\n')
            f.write(str(ident))
            f.write('\n-----\n')
            for stat in stats[ident]:
                f.write(str(stat))
                f.write(':\t')
                f.write(str(stats[ident][stat]))
                f.write('\n')
                f.flush()

    def finalise(self):
        if self.stdout:
            f = sys.stdout
        else:
            f = open(self.fname, 'w')

        stats = self._gen_stats()
        self._write_stats(f, stats)

        if not self.stdout:
            f.close()

    def register_point(self, ident, ecef):
        with self.point_lock:
            if not ident in self.points:
                self.points[ident] = []

            self.points[ident].append(ecef)
