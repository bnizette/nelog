
'''
uBlox GPS module representation and communications interface

Copyright Ben Nizette April 2013

This file is part of pyNelog.

pyNelog is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyNelog is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyNelog.  If not, see <http://www.gnu.org/licenses/>.
'''

import serial
import threading

from time import sleep

import UBX_Message as ubx


class PortException(Exception):
    pass


class UBX_Module:
    def __init__(self, port, ident, loc, logfile=None):
        self.port = serial.Serial(port)

        if not self.port.isOpen():
            raise PortException("Can't open {}".format(port))

        self.terminate = False

        self.ident = ident

        self.eph = loc.eph
        self.loc = loc

        self.healthmask = 0

        self.ubx_configure_messages()

        self.rxthread = threading.Thread(target=self.rx_thread_fn)
        self.pollthread = threading.Thread(target=self.poll_thread_fn)

        self.rxthread.start()
        self.pollthread.start()
        self.logfile = None
        if logfile:
            self.logfile = logfile + "-MOD-" + str(ident) + ".dump"


    def _write_logfile(self, line):
        if self.logfile is None:
            return

        with open(self.logfile, 'a') as f:
            f.write(line)


    def _write_msg(self, msg):
        self.port.write(msg.binstr())
        self.port.flush()

    def ubx_configure_messages(self):
        dis_all = b'\x00\x00\x00\x00\x00\x00'
        # Disable all NMEA messages
        self._write_msg(ubx.ConfigureMessage('GGA', dis_all))
        self._write_msg(ubx.ConfigureMessage('GLL', dis_all))
        self._write_msg(ubx.ConfigureMessage('GSA', dis_all))
        self._write_msg(ubx.ConfigureMessage('GSV', dis_all))
        self._write_msg(ubx.ConfigureMessage('RMC', dis_all))
        self._write_msg(ubx.ConfigureMessage('VTG', dis_all))
        self._write_msg(ubx.ConfigureMessage('ZDA', dis_all))

        # Turn the module's internal filtering down as low as
        # possible (aircraft mode, 4G accel := dyn mode 7)
        self._write_msg(ubx.NAV5Message(dyn=b'\x07'))

        # Get raw position at 10Hz
        self._write_msg(ubx.RateMessage(100))
        self._write_msg(ubx.ConfigureMessage('RXM-RAW', b'\x01'))
        self._write_msg(ubx.ConfigureMessage('NAV-SOL', b'\x01'))

    def dispatch_received(self, msg):
        if msg.msgid == ubx.msgtypes['NAV-SOL']:
            #print("Sol")
            X = ubx.I4(msg.payload[12:16])
            Y = ubx.I4(msg.payload[16:20])
            Z = ubx.I4(msg.payload[20:24])
            self._write_logfile("SOL {} {} {}\n".format(X, Y, Z))
            self.loc.load_mod_solution([X / 100, Y / 100, Z / 100])
        elif msg.msgid == ubx.msgtypes['RXM-RAW']:
            #print("Raw")
            t = ubx.I4(msg.payload[0:4]) / 1000.0  # ms to secs

            nsv = ubx.U1(msg.payload[6:7])

            self.loc.start_load()

            for i in range(0, nsv):
                ph = ubx.R8(msg.payload[8 + (24*i): 16 + (24*i)])
                pr = ubx.R8(msg.payload[16 + (24*i): 24 + (24*i)])
                dopp = ubx.R4(msg.payload[24 + (24*i): 28 + (24*i)])
                sv = ubx.U1(msg.payload[28 + (24*i): 29 + (24*i)])
                qi = ubx.I1(msg.payload[29 + (24*i): 30 + (24*i)])

                # TODO: qi < 6 indicates lock loss, does this munge phase?
                # if (qi < 6):
                #    ph = None

                # Only register the measurement if the PR and DO are valid
                if (qi >= 4):
                    self._write_logfile("RAW {} {} {} {} {}\n".format(sv, t, pr, dopp, ph))
                    self.loc.load_raw_entry(sv, t, pr, dopp, ph)

            self.loc.end_load()

        elif msg.msgid == ubx.msgtypes['AID-EPH']:
            #print("module {} Ephemeris {}".format(self.ident, msg.length))
            if msg.length == 104:
                sv = ubx.U4(msg.payload[0:4])
                self._write_logfile("EPH {}\n".format(sv))
                d = [0] * 24
                for i in range(0, 24):
                    d[i] = ubx.U4(msg.payload[8 + i*4: 13 + i*4])

                # Takes an array of ints representing the raw subframes
                self.eph.load_eph_entry(sv, d)

        elif msg.msgid == ubx.msgtypes['AID-HUI']:
            #print("Ionosphere")
            if msg.length == 72:
                hm = ubx.U4(msg.payload[0:4])
                a0 = ubx.R4(msg.payload[36:40])
                a1 = ubx.R4(msg.payload[40:44])
                a2 = ubx.R4(msg.payload[44:48])
                a3 = ubx.R4(msg.payload[48:52])
                b0 = ubx.R4(msg.payload[52:56])
                b1 = ubx.R4(msg.payload[56:60])
                b2 = ubx.R4(msg.payload[60:64])
                b3 = ubx.R4(msg.payload[64:68])

                flags = ubx.X4(msg.payload[68:72])

                if (flags & 1):
                    self.healthmask = hm

                if (flags & 4):
                    self._write_logfile("ION {}\n".format([a0, a1, a2, a3, b0, b1, b2, b3]))
                    self.loc.load_ionospheric_entry(
                        [a0, a1, a2, a3, b0, b1, b2, b3])
            else:
                print('Queer HUI length {}'.format(msg.length))

        elif msg.msgid == ubx.msgtypes['ACK-ACK']:
            pass  # TODO: These should actually notify the originating message of status
        elif msg.msgid == ubx.msgtypes['ACK-NAK']:
            pass
        else:
            print('Unknown message received: {}'.format(msg))

    def rx_thread_fn(self):
        while not self.terminate:
            b = self.port.read()
            if b[0] != ubx.sync[0]:
                continue

            b = self.port.read()
            if b[0] != ubx.sync[1]:
                continue

            msg_id = self.port.read(2)
            pay_len = self.port.read(2)
            int_len = ubx.U2(pay_len)

            payload = self.port.read(int_len)
            chk = self.port.read(2)

            m = ubx.Message()
            m.msgid = msg_id
            m.payload = payload
            m.length = int_len

            calc_chk = m.checksum(m.bytes()[2:-2])

            # if calc_chk == chk:
            self.dispatch_received(m)
            # else:
            #    print('Failed Checksum')

    def poll_thread_fn(self):
        while not self.terminate:
            # Poll aiding messages at 0.1Hz, the responses
            # will be picked up by the normal rx thread.
            # Note that this thread only writes, the other
            # only reads, no locking is required.
            self._write_msg(ubx.PollMessage('AID-HUI'))
            self._write_msg(ubx.PollMessage('AID-EPH'))

            sleep(10)

    def stop(self):
        self.terminate = True
        self.rxthread.join()
        self.pollthread.join()
        self.port.close()
